<?php

namespace Core\Controller;


class Controller
{
    protected $viewsPath;
    protected $template;

    protected function render($view, $variables = []){
        ob_start();
        extract($variables);
        require($this->viewsPath.str_replace('.', '/', $view).'.php');

        $content = ob_get_clean();
        require($this->viewsPath.'templates/'.$this->template.'.php');
    }

    protected function forbidden(){
        header('HTTP/1.0 403 forbidden');
        die('Acces interdit!');
    }

    protected function notFound(){
        header();
        die('Page introuvable');
    }

}