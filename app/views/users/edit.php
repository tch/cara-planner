<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <?php if($empty): ?>
            <div class="alert alert-danger">
                Tous les champs doivent être remplis.
            </div>
        <?php endif; ?>

        <?php if($preg): ?>
            <div class="alert alert-danger">
                La date doit être au format : dd/mm/yyyy.
            </div>
        <?php endif; ?>
        <br>
        <br>
        <h2 class="text-center">Modifier un événement</h2>
        <br>
        <br>
        <form method="post">
            <div class="text-center">
                <?= $form->input('title', 'Description de l\'événement', ['type' => 'textarea']); ?>
                <label>A quelle date ?</label>
                <input type="text" name="datepicker" class="form-control" value="" id="datepicker">
                <br>
                <button class="btn btn-primary">Modifier</button>
            </div>
        </form>
        <br>
        <br>
        <a href="?p=plan.index" class="btn btn-success">Retour à l'accueil</a>

    </div>
</div>
<script>
      ;(function($){
        $.fn.datepicker.dates['fr'] = {
            days: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
            daysShort: ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
            daysMin: ["d", "l", "ma", "me", "j", "v", "s"],
            months: ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"],
            monthsShort: ["janv.", "févr.", "mars", "avril", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc."],
            today: "Aujourd'hui",
            clear: "Effacer",
            weekStart: 1,
            format: "dd/mm/yyyy"
        };
    }(jQuery));

    $(document).ready(function() {
        $("#datepicker").datepicker({
            format: "dd/mm/yyyy",
            language: 'fr'
        }).val();

    });
</script>