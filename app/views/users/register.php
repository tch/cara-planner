<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">

        <?php if($errors): ?>
            <div class="alert alert-danger">
                Utilisateur déjà existant.
            </div>
        <?php endif; ?>

        <?php if($errorsR): ?>
            <div class="alert alert-danger">
                Erreur lors de l'inscription.
            </div>
        <?php endif; ?>

        <?php if($empty): ?>
            <div class="alert alert-danger">
                Tous les champs doivent être remplis.
            </div>
        <?php endif; ?>

        <?php if($preg): ?>
            <div class="alert alert-danger">
                Veuillez entrer des caractères valides.
            </div>
        <?php endif; ?>

        <form method="post">
            <div class="text-center">
                <h1>Inscription à Lil'-Planner</h1>
                <br>
                <?= $form->input('username', 'Pseudo'); ?>
                <?= $form->input('password', 'Mot de passe', ['type' => 'password']); ?>
                <button class="btn btn-primary">Inscription</button>
            </div>
        </form>

    </div>
</div>