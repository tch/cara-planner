<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <br>
        <br>
        <h2 class="text-center">Bienvenue sur Lil'-planner !</h2>
        <br>
        <br>
        <p class="alert alert-success">
            Bravo vous êtes maintenant inscrit !
        </p>
        <p>
            Vous pouvez dès maintenant vous connecter ici : <a class="btn btn-primary" href="?p=users.login">Connexion</a>
        </p>
        <br>
        <br>
        <a href="?p=plan.index" class="btn btn-success">Retour à l'accueil</a>

    </div>
</div>