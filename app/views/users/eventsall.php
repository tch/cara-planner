<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">

        <?php if(isset($message)): ?>
        <h1>Vous n'avez pas encore d'événement</h1>
            <br>
        <p>Vous pouvez en rajouter ici : <a class="btn btn-primary" href="?p=users.addevent">Ajouter événement</a></p>
        <?php elseif(isset($events)): ?>
            <br>
            <br>
            <h2>Voici la liste de vos événements : </h2>
            <br>
            <table class="table">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>

                </thead>
                <tbody>

        <?php foreach($events as $event): ?>

            <?php $date = new DateTime(htmlspecialchars($event->date))?>
                <tr>
                <td><?= $date->format('d-m-Y'); ?></td>
                <td><?= htmlspecialchars(substr($event->title, 0, 25)).'...'; ?></td>
                <td>
                    <a style="margin-bottom: 10px;" href="?p=users.edit&id=<?= $event->id; ?>" class="btn btn-warning">Modifier</a>
                    <form method="post" action="?p=users.deleteevent" style="display: inline;">
                    <input type="hidden" name="id" value="<?= $event->id; ?>">
                    <button type="submit" class="btn btn-danger">Supprimer</button>
                    </form>


                </td>

                </tr>
        <?php endforeach; ?>

                </tbody>
            </table>

        <?php endif; ?>
        <br>
        <a href="?p=plan.index" class="btn btn-success">Retour à l'accueil</a>


    </div>
</div>