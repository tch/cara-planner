<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">

        <?php if($errors): ?>
            <div class="alert alert-danger">
                Identifiants incorrects
            </div>
        <?php endif; ?>

        <?php if($empty): ?>
            <div class="alert alert-danger">
                Tous les champs doivent être remplis.
            </div>
        <?php endif; ?>

        <?php if($preg): ?>
            <div class="alert alert-danger">
                Votre pseudo doit utiliser des caractères valides.
            </div>
        <?php endif; ?>



        <form method="post">
            <div class="text-center">
                <h1>Connexion à Lil'-Planner</h1>
                <br>
                <?= $form->input('username', 'Pseudo'); ?>
                <?= $form->input('password', 'Mot de passe', ['type' => 'password']); ?>
                <button class="btn btn-primary">Connexion</button>
            </div>
        </form>

    </div>
</div>