<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <br>
        <br>
        <h2 class="text-center">Contact</h2>
        <br>
        <br>
        <div class="jumbotron">
        <p>
            Pour tout problème, vous pouvez nous contacter :  <a href="mailto:lilplannercontact@gmail.com">lilplannercontact@gmail.com</a>
        </p>
            </div>
        <br>
        <br>
        <a href="?p=plan.index" class="btn btn-success">Retour à l'accueil</a>

    </div>
</div>