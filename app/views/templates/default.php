<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="lil'-planner">
    <meta name="author" content="Cara">
    <title><?= App::getInstance()->title ?></title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
    <!-- <script type="text/javascript" src="http://jqueryui.com/resources/demos/datepicker/datepicker-fr.js"></script>
        -->

</head>
<body style="margin: auto;">
<div class="container">
<h1><span class="glyphicon glyphicon-calendar"></span><a id="none" href="?p=plan.index">Lil-Planner.fr</a>
</h1>
    <small>Mini calendrier pour vous organiser simplement. (Seulement l'année en cours pendant l'alpha)</small>
<br>


<?php if(isset($_SESSION['auth'])): ?>
<div id="nav">
    <h3>Bonjour, <?= htmlspecialchars(ucfirst($_SESSION['username'][0])) ?></h3>
    <a href="?p=users.addevent"><h4>Ajouter un événement</h4></a>
    <a href="?p=users.eventsall"><h4>Vos événements</h4></a>
    <a href="?p=users.getprofil"><h4>Votre profil</h4></a>
    <a href="?p=users.getgroups"><h4>Vos groupes</h4></a>
    <?php if((int)$_SESSION['role'][0] === 3): ?>
    <a href="#"><h4>Administration</h4></a>
    <?php endif; ?>
    <a href="?p=users.logout"><h4>Déconnexion</h4></a>
    <!-- <a href="?p=plan.testmail"><h4>test mail</h4></a> -->
</div>
<?php else :?>
    <div id="nav">
        <h3>Bonjour, invité</h3>
        <a href="index.php?p=users.login"><h4>Connexion</h4></a>
        <a href="index.php?p=users.register"><h4>S'inscrire</h4></a>
    </div>

<?php endif; ?>
<br>
<br>
<br>
<br>
<br>
<?= $content ?>


</div>
<div class="clear"></div>

</body>
<br>
<footer class="foot">Lil'-Planner © 2015 | Alpha V0.1 | Merci à : <a href="http://www.grafikart.fr">Grafikart</a> | <a href="index.php?p=plan.mentions">Mentions Légales</a> | <a href="index.php?p=plan.contact">Contact</a></footer>

</html>