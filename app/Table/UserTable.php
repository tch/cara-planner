<?php
namespace App\Table;

use Core\Table\Table;
use \App;

class UserTable extends Table
{
    protected $table = "users";
    /**
     * trouver si un user existe déjà
     *
     */
    public function isExist($username){

        $result = $this->query("
        SELECT users.username
        FROM {$this->table}
        WHERE users.username = ?
        ", [strtolower($username)], true);

        return $result;
    }

}