<?php

namespace App\Table;

use Core\Table\Table;

class EventTable extends Table
{
    protected $table = "events";


    /**
     * recupérer tous les events d'un user
     * @param $user_id
     * return App\Entity\EventEntity
     *
     */
    public function allEvent($user_id){
        return $this->query(
            "SELECT *
            FROM {$this->table}
            WHERE user_id = ?
            ORDER BY events.date DESC ", [$user_id], true);
    }

}