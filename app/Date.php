<?php
namespace App;

use \App;
use \PDO;
use \DateTime;
use \DateInterval;

class Date{

    var $days = array('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche');
    var $months = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre',
        'Octobre', 'Novembre', 'Décembre');

    protected $db;

    /**
     * @return PDO
     * récupérer pdo juste pour la gestion des Dates
     */

    public function getDb(){
        $this->db = new PDO('mysql:host=localhost;dbname=planner', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'));
        $this->db->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES utf-8');
        return $this->db;
    }


    /**
     * @param $year
     * @return array
     * récupérer les events d'un users donné
     *
     */

    public function getEvents($year, $user_id){

        #TODO gestion heures

        $req = $this->getDb()->query("SELECT events.id, events.title, date, events.user_id
        FROM events
        WHERE user_id=".$user_id." AND YEAR(date)=".$year);

        $retour = array();
        while($d = $req->fetch(PDO::FETCH_OBJ)){

            $retour[strtotime($d->date)][$d->id] = $d->title;

        }

        return $retour;
    }

    public  function getAll($year){
        $retour = array();
        /**
         *
         *
         $date = strtotime($year.'-01-01');
         while(date('Y', $date) <= $year) {
            $y = date('Y', $date);
            $m = date('n', $date);
            $d = date('j', $date);
            $w = str_replace('0', '7', date('w', $date));

            $retour[$y][$m][$d] = [$w];
            $date = strtotime(date('Y-m-d', $date).' +1 DAY');
        }
         *
         */
        $date = new Datetime($year.'-01-01');
        while($date->format('Y') <= $year) {
            $y = $date->format('Y');
            $m = $date->format('n');
            $d = $date->format('j');
            $w = str_replace('0', '7', $date->format('w'));

            $retour[$y][$m][$d] = [$w];
            $date->add(new DateInterval('P1D'));
        }

        return $retour;
    }

    public function getMoisCurrent(){
        return date('m');
    }


}