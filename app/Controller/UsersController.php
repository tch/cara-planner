<?php

namespace App\Controller;

use Core\Auth\DatabaseAuth;

use Core\HTML\BootstrapForm;
use \App;

class UsersController extends AppController
{
    public function __construct(){

        parent::__construct();
        $this->loadModel('Event');
        $this->loadModel('User');
    }

    public function success(){
        $this->render('users.success');
    }

    public function login(){
        if(isset($_SESSION['auth'])){
            header('Location: index.php?p=plan.index');
        }

        $errors = false;
        $empty = false;
        $preg = false;
        if(!empty($_POST)){
            if(empty($_POST['username']) || empty($_POST['password'])){
                $empty = true;
            }else {
                if (preg_match("/[^a-zA-Z0-9_]/", $_POST['username'])) {
                    $preg = true;
                } else {

                    $auth = new DatabaseAuth(App::getInstance()->getDb());
                    if ($auth->login($_POST['username'], $_POST['password'])) {
                        header('Location: index.php?p=plan.index');
                    } else {
                        $errors = true;
                    }
                }
            }
        }
        $form = new BootstrapForm($_POST);

        $this->render('users.login', compact('form', 'errors', 'empty', 'preg'));
    }

    public function register(){
        if(isset($_SESSION['auth'])){
            header('Location: index.php?p=plan.index');
        }

        $errors = false;
        $errorsR = false;
        $empty = false;
        $preg = false;
        $form = new BootstrapForm($_POST);
        if(!empty($_POST)){
            if(empty($_POST['username']) || empty($_POST['password'])){
                $empty = true;
            }else {
                if(preg_match("/[^a-zA-Z0-9_]/", $_POST['username'])){
                    $preg = true;
                }else {
                    if ($this->User->isExist($_POST['username'])) {
                        $errors = true;
                    } else {
                        $password = sha1($_POST['password']);
                        $username = $_POST['username'];
                        $role = 1;
                        $result = $this->User->create([
                                'username' => $username,
                                'password' => $password,
                                'role' => $role,
                            ]
                        );
                        if ($result) {
                             $this->success();
                             exit();
                            //header('Location : index.php?p=plan.index');
                        } else {
                            $errorsR = true;
                        }

                    }
                }
            }

        }


       $this->render('users.register', compact('form', 'errors', 'errorsR', 'empty', 'preg'));

    }

    public function logout(){
        if(!isset($_SESSION['auth'])){
            header('Location: index.php?p=plan.index');
        }

        $auth = new DatabaseAuth(App::getInstance()->getDb());
        if($auth->logged()){
            session_destroy();
            header('Location: index.php?p=plan.index');

        }

    }

    public function deleteEvent(){
        if(!isset($_SESSION['auth'])){
            header('Location: index.php?p=plan.index');
        }
        if(!empty($_POST)){
            $result = $this->Event->delete($_POST['id']);
            header('Location: index.php?p=users.eventsall');
        }
    }

    public function edit(){
        if(!isset($_SESSION['auth'])){
            header('Location: index.php?p=plan.index');
        }
        $empty = false;
        $preg = false;
        if(!empty($_POST)) {
            if(empty($_POST['title']) || empty($_POST['datepicker'])) {
               $empty = true;
            } else {
                if (preg_match("/^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4}$/", $_POST['datepicker'])) {
                    $preg = true;

                } else {

                    //pour convertir au format date mysql

                    $datePost = $_POST['datepicker'];
                    $day = substr($datePost, 0, 2);
                    $month = substr($datePost, 3, 2);
                    $year = substr($datePost, 6, 4);
                    $timestamp = mktime(0, 0, 0, $month, $day, $year);
                    $date = date('Y-m-d', $timestamp);

                    $result = $this->Event->update($_GET['id'], [
                        'title' => $_POST['title'],
                        'date' => $date,
                    ]);
                    if ($result) {
                        header('Location: index.php?p=users.eventsall');
                    }

                }
            }
        }


        $event = $this->Event->find($_GET['id']);
        $form = new BootstrapForm($event[0]);

        $this->render('users.edit', compact('form', 'empty', 'preg'));
    }

    public function addEvent(){
        if(!isset($_SESSION['auth'])){
            header('Location: index.php?p=plan.index');
        }
        $empty = false;
        $preg = false;
        $form = new BootstrapForm($_POST);
        if(!empty($_POST)) {
            if (empty($_POST['title']) || empty($_POST['datepicker'])) {
                $empty = true;

            } else {
                if (preg_match("/^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4}$/", $_POST['datepicker'])) {
                    $preg = true;

                } else {
                    //pour convertir au format date mysql

                    $datePost = $_POST['datepicker'];
                    $day = substr($datePost, 0, 2);
                    $month = substr($datePost, 3, 2);
                    $year = substr($datePost, 6, 4);
                    $timestamp = mktime(0, 0, 0, $month, $day, $year);
                    $date = date('Y-m-d', $timestamp);

                    $result = $this->Event->create([
                        'title' => $_POST['title'],
                        'date' => $date,
                        'user_id' => $_POST['user_id'],
                    ]);
                    if ($result) {
                        // header('Location: admin.php?p=posts.edit&id='.App::getInstance()->getDb()->lastInsertId());
                        header('Location: index.php?p=plan.index');
                    }
                }
            }
        }


        $this->render('users.add', compact('form', 'empty', 'preg'));
    }

    public function eventsAll(){
        if(!isset($_SESSION['auth'])){
            header('Location: index.php?p=plan.index');
        }

        $user_id = null;
        if(isset($_SESSION['auth'])){
            $user_id = $_SESSION['auth'][0];
            $events = $this->Event->allEvent($user_id);
            if(empty($events)){
                $message = "Vous n'avez pas d'événements";
                $this->render('users.eventsall', compact('message'));
            }else{
                $this->render('users.eventsall', compact('events'));
            }
        }
    }

    public function getProfil(){
        if(!isset($_SESSION['auth'])){
            header('Location: index.php?p=plan.index');
        }

        $this->render('users.profil');
    }

    public function getGroups(){
        if(!isset($_SESSION['auth'])){
            header('Location: index.php?p=plan.index');
        }

        $this->render('users.groups');
    }


}