<?php

namespace App\Controller;
use \App;
use Core\Controller\Controller;
use Core\Auth\DatabaseAuth;

class AppController extends Controller
{
    protected $template = 'default';

    public function __construct(){
        $this->viewsPath = ROOT.'/app/views/';
       /* $app = App::getInstance();
        $auth = new DatabaseAuth($app->getDb());
        if(!$auth->logged()){
           // $this->forbidden();
           header('Location: login.php');
        }*/
    }

    public function loadModel($modelName){
        $this->$modelName = App::getInstance()->getTable($modelName);
    }

}