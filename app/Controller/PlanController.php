<?php

namespace App\Controller;


use App\Date;

class PlanController extends AppController
{
    public function __construct(){

        parent::__construct();
        $this->loadModel('Event');
        $this->loadModel('User');
    }

    public function index(){
        $date = new Date();
        $year = date('Y');
        $dates = $date->getAll($year);
        //pour récupérer le mois en cours
        $moisCurrent = $date->getMoisCurrent();
        $user_id = null;
        if(isset($_SESSION['auth'])) {
            $user_id = $_SESSION['auth'][0];
            $events = $date->getEvents($year, $user_id);
            $this->render('planner.index', compact('year', 'date', 'events', 'dates', 'moisCurrent'));
        }else {
            $this->render('planner.index', compact('year', 'date', 'dates', 'moisCurrent'));
        }
    }

    public function contact(){
        $this->render('planner.contact');
    }

    public function mentions(){
        $this->render('planner.mentions');
    }

   /* public function testMail(){
        $to      = 'testdevmail62@gmail.com';
        $subject = 'le sujet';
        $message = 'Bonjour !';
        $headers = 'From: webmaster@example.com' . "\r\n" .
            'Reply-To: webmaster@example.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $message, $headers);

    }
        */


}